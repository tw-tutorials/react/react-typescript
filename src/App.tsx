import React from 'react';
import './App.css';
import MultiInputComponent from './components/MultiInputComponent';

const App: React.FC = () => {
	return (
		<div className="App">
			{/* <PokemonSearch name="John Doe" numberOfPokemons={5}></PokemonSearch> */}
			<MultiInputComponent />
		</div>
	);
};

export default App;
