import React, { Component } from 'react';

interface ComponentProps {}

interface State {}

class MultiInputComponent extends Component<ComponentProps, State> {
	firstNameRef: React.RefObject<HTMLInputElement>;
	lastNameRef: React.RefObject<HTMLInputElement>;

	constructor(props: ComponentProps) {
		super(props);

		// Bind refs
		this.firstNameRef = React.createRef();
		this.lastNameRef = React.createRef();
	}

	onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		const firstName = this.firstNameRef.current.value;
		const lastName = this.lastNameRef.current.value;
		console.log(`Hello ${firstName} ${lastName}`);
	};

	render() {
		return (
			<form onSubmit={this.onSubmit}>
				<input
					type="text"
					ref={this.firstNameRef}
					name="firstname"
					placeholder="First name"
				/>
				<input type="text" ref={this.lastNameRef} name="lastname" placeholder="Last name" />

				<button type="submit">Submit</button>
			</form>
		);
	}
}

export default MultiInputComponent;
